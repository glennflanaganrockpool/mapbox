/**
 * Twitter Streaming APIs Demos.
 * Romain Huet
 * @romainhuet
 */

'use strict';

var fs = require('fs');
var path = require('path');
var server = require('http').createServer(handler);
var io = require('socket.io').listen(server);
var Twitter = require('twit');
var config = require('./config');
var https = require('https');


// Twitter client initialization.
var twitter = new Twitter(config);

var ukScore = 0;
var usaScore = 0;
var tweets = [];
var stream;


// Handler for the server to return the proper HTML files.


//nw x se [lat,long][lat,long]
var usaBounds = [[48.740172, -127.705906],[24.515031, -66.446139]];
var ukBounds = [[62.692177, -9.181596],[50.733514, 2.507858]];


initServer();

io.set('log level', 0);

// Socket connection.
io.sockets.on('connection', function(socket) {

  //console.log(socket);

  // Listen to the `stream` event.
  socket.on('stream', function(params) {
    var endpoint = params.endpoint || 'statuses/filter';
    var options = {};
    if (params.keywords) {
      options.track = params.keywords;
    }
    if (params.locations) {
      options.locations = params.locations;
    }
    if (params.users) {
      options.follow = params.users;
    }
    if (params.language) {
      options.language = params.language;
    }

    // Subscribe to the stream of Tweets matching the specified options.
    var stream = twitter.stream(endpoint, options);

    // Listen to the `connect` event.
    stream.on('connect', function(params) {
      console.log('Streaming from the Twitter API...');
    });

    // Emit an event with the Tweet information.
    stream.on('tweet', function(tweet) {
      io.sockets.emit('tweet', tweet);
    });

    // Listen to the `disconnect`/`stop` events to destroy the connection.
    socket.on('disconnect', function(params) {
      console.log('Streaming ended (disconnected).');
      stream.stop();
    });
    socket.on('stop', function(params) {
      console.log('Streaming ended (stopped).');
      stream.stop();
    });
  });

  socket.on('citystream', function(params){

    io.to(socket.id).emit('initdata', {ukScore: ukScore, usaScore: usaScore, tweetPairs: tweets})

    

  });

  socket.on('static', function(params){
    
    console.log('Static Search initiated');

    var endpoint = params.endpoint || 'search/tweets';
    
    //default to recent tweets.
    var result_type = params.result_type || 'recent';

    // default to 20km of London, UK
    var geosearch = params.geocode || [51.508774, -0.127348, '20km'];

    var staticSearch = twitter.get(endpoint, {q: '%23santa', result_type: result_type, geocode: geosearch, count: 100}, function(err, data, response){
      if(data){
        io.sockets.emit('staticdata', data);
        console.log('London Search Results send to client');
      }
    });

    geosearch = [40.690529, -73.985173, '20km'];

    var secondSearch = twitter.get(endpoint, {q: '%23santa', result_type: result_type, geocode: geosearch, count: 100}, function(err, data, response){
      if(data){
        io.sockets.emit('staticdata', data);
        console.log('New York Search Results send to client');
      }
    });

  });


});

function initServer(){
  // Listen on port 3000.
  server.listen(3000);
  console.log('Listening on port 3000');

  var endpoint = 'statuses/filter';
  var options = {track: 'santa'};


  // Subscribe to the stream of Tweets matching the specified options.
  stream = twitter.stream(endpoint, options);

  // Listen to the `connect` event.
  stream.on('connect', function(params) {
    console.log('Streaming from the Twitter API...');
  });

  // Emit an event with the Tweet information.
  stream.on('tweet', function(tweet) {
      var geocoordinates = [];

      //if tweet has exact geolocation
      if(tweet.geo){
        geocoordinates = [tweet.geo.coordinates[0],tweet.geo.coordinates[1]];

        logTweet(tweet, geocoordinates);
      }
      else{
        var url = "https://maps.googleapis.com/maps/api/geocode/json?address="+tweet.user.location+"&key=AIzaSyA7ZmIqLgqisLpbzBf5yhsebs3hF6627Yc"
        var req = https.get(url, function(response){
          var str = "";
          response.on('data', function (chunk) {
            str += chunk;
          });

          response.on('end', function () {
            //console.log(req.data);
            //console.log(str);

            var georesults = JSON.parse(str);

            if(georesults.status == 'OK'){
              var result = georesults.results[0];
              geocoordinates = [result.geometry.location.lat, result.geometry.location.lng];

              logTweet(tweet, geocoordinates);
              //console.log(geocoordinates);
            }
            else{
              //console.log('No location found');
            }

            //console.log(georesults);
              // you        
          });

        }).on('error', function(e){
          console.log('Error: ' + e.message);
        });

        

      }

      
    });
}


 // now we have the geo cooridnate of the tweet we need to check if they fall within a given area on the map
function logTweet(tweet, geocoordinates){
  if( isTweetInBounds(ukBounds, geocoordinates) ){
    io.sockets.emit('tweetUK', {tweet: tweet, coordinates: geocoordinates});
    tweets.push({tweet: tweet, coordinates: geocoordinates});
    ukScore++;

  }else if ( isTweetInBounds(usaBounds, geocoordinates) ){
    io.sockets.emit('tweetUSA', {tweet: tweet, coordinates: geocoordinates});
    tweets.push({tweet: tweet, coordinates: geocoordinates});
    usaScore++;

  }else{
    //io.sockets.emit('nolocation', tweet);
  }

  console.log('SCORES: UK - '+ukScore+' : USA: '+usaScore);
}

function isTweetInBounds(bounds, tweetCoords){

  //LAT IS Y, LONG IS X!!
  
  //[top, bottom];
  var latRange = [bounds[0][0], bounds[1][0]];
  //[left, right];
  var longRange = [bounds[0][1], bounds[1][1]];

  var tweetLat = tweetCoords[0];
  var tweetLong = tweetCoords[1];


  if(tweetLat <= latRange[0] && tweetLat >= latRange[1] && tweetLong >= longRange[0] && tweetLong <= longRange[1]){
    //tweet coordinates are wihin range
    return true;
  }
  else{
    return false;
  }

}

function handler(req, res) {
  var file = path.join(__dirname, 'count.html');
  if (req.url.indexOf('custom') !== -1) {
    file = path.join(__dirname, 'custom.html');
  } else if (req.url.indexOf('geolocation') !== -1) {
    file = path.join(__dirname, 'geolocation.html');
  } else if (req.url.indexOf('assets') !== -1) {
    file = path.join(path.dirname(__dirname), req.url);
  }
  fs.readFile(file, function(err, data) {
    if (err) {
      res.writeHead(404);
    } else {
      res.writeHead(200);
      res.end(data);
    }
  });
}